
// Lectura sincrona de archivos
const fs = require('fs');

//Declaracion de la funcion
const getFile = fileName => {
    return new Promise((resolve, reject) => {
        //El trabajo se hace dentro de la promesa
        fs.readFile(fileName, (err,data) => {
            if(err) {
                reject(err);
                return;
            }
            resolve(data);
        });
    });
}

//Utilización de la promesa
getFile('20190911.json')
    .then(JSON.parse)
    .then(console.log)
    .catch(console.error)